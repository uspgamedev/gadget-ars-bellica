extends Camera


export var player_path: NodePath


func _process(_delta):
	var player := get_node(player_path) as Spatial
	look_at(player.translation, Vector3.UP)
