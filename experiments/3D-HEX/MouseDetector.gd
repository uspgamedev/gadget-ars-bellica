extends Area

export var map_path: NodePath

signal cell_event(event, cell)

func _on_input_event(_camera, event, click_position, _click_normal, _shape_idx):
	var map: HexMap = get_node(map_path)
	var cell := map.find_closest_cell(click_position)
	emit_signal("cell_event", event, cell)
