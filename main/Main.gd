extends Control

export var first_state_class: Script

onready var game := Game.new()
onready var stack := Stack.new()

var auto_connection: AutoConnection

func _ready():
	# Init game struct
	game.simulation = $Simulation
	game.view = $View
	game.ui = $UI
	
	# Push first state and begin running
	stack.push(first_state_class.new(), game)
	
	# Watch for "control_requested" signals
	auto_connection = AutoConnection.new(Requester, "requested", self,
		"_on_requested")
	auto_connection.traverse_and_connect(self)
	auto_connection.watch(get_tree())

func _exit_tree():
	stack.free()
	game.free()

func _on_requested(event: String, request: Request):
	if not stack.empty():
		var state := stack.current()
		var method_name = "_on_" + event
		if state.has_method(method_name):
			state.call(method_name, request)

func _process(_delta):
	if stack.empty():
		get_tree().quit()

func _physics_process(delta):
	var tick_request := TickRequest.new()
	tick_request.delta = delta
	_on_requested("physics_tick", tick_request)
