class_name TitleMenu extends State

func _enter(_previous: State):
	game.ui.main_menu.show()

func _on_new_game(_request: Request):
	TransitionManager.begin_transition()
	yield(TransitionManager, "screen_dimmed")
	switch(Constants.campaign_state.new())
	TransitionManager.end_transition()

func _on_continue(_request: Request):
	pass

func _on_exit(_request: Request):
	TransitionManager.begin_transition()
	yield(TransitionManager, "screen_dimmed")
	pop()
	TransitionManager.end_transition()

func _leave():
	game.ui.main_menu.hide()
