class_name TacticalMode extends GameplayMode

var player_cur_hex := Vector2.ZERO
var is_player_centered : bool = true

func _enter(previous: State):
	assert(map != null)
	if previous is TurnTracking:
		# Initialize simulation
		preload_entities()
	map.get_hex_wireframe().show()
	var player: ControlledBodyView = map.get_node_or_null(player_view_path)
	assert(player != null)
	var hex_grid := map.get_hex_grid()
	var cell := hex_grid.find_closest_cell(player.global_transform.origin)
	player_cur_hex = cell.get_hex()
	yield(player.slide_to_position(hex_grid), "completed")
	snap_bounds_to_player(player)
	print("tactical")

func _leave():
	map.unhighlight_hex()
	map.get_hex_wireframe().hide()
	map.get_movement_bounds().enabled = false

func _on_toggle_mode(_request: Request):
	var player: ControlledBodyView = map.get_node_or_null(player_view_path)
	if(player.get_tween_active()):
		return
	var exploration_mode = Constants.exploration_mode_state.new()
	exploration_mode.map = map
	exploration_mode.player_view_path = player_view_path
	switch(exploration_mode)

func _on_physics_tick(tick_request: TickRequest):
	var player: ControlledBodyView = map.get_node(player_view_path)
	assert(player != null)
	
	_update_bodyview_positions()
	ActionProtocol.advance_turns(game.simulation)
	
	map.unhighlight_hex()
	player.control_movement(map.get_hex_grid(), tick_request.delta,
							ControlledBodyView.AUTO_SLIDE)
	
	if player.get_tween_active():
		map.get_movement_bounds().enabled = false
	else:
		if not map.get_movement_bounds().enabled:
			snap_bounds_to_player(player)
		if player.has_focused_hex():
			var hex := player.get_focused_hex(map.get_hex_grid())
			if ActionProtocol.is_tile_blocked_for(game.simulation, player.entity_id,
												  hex):
				map.highlight_hex(hex, Map.HIGHLIGHT_INVALID)
			else:
				map.highlight_hex(hex, Map.HIGHLIGHT_VALID)

func _on_interact(_request: Request):
	var player: ControlledBodyView = map.get_node(player_view_path)
	if(player.get_tween_active() || not player.has_focused_hex()):
		return
	var hex_grid := map.get_hex_grid()
	var focused_hex := player.get_focused_hex(hex_grid)
	
	if ActionProtocol.is_tile_blocked_for(game.simulation, player.entity_id,
										  focused_hex):
		# try to attack
		ActionProtocol.try_attack(game.simulation, player.entity_id, focused_hex)
	else:
		# move
		ActionProtocol.commit_movement(game.simulation, player.entity_id, focused_hex)

func _on_action(request: ActionRequest):
	var player: ControlledBodyView = map.get_node(player_view_path)
	EntityAccess.set_player_action(game.simulation, player.entity_id,
								   request.action)

func snap_bounds_to_player(player: BodyView):
	var bounds := map.get_movement_bounds()
	var hex = game.simulation.find_entity(player.entity_id).find_property(Body).hex
	bounds.enabled = true
	bounds.translation = map.get_hex_grid().qr2position(hex.x, hex.y)
