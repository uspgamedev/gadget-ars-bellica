class_name GameplayMode extends State

var map: Map = null
var player_view_path := NodePath()

func _on_entity_state_changed(request: EntityFXRequest):
	var entity := game.simulation.find_entity(request.entity_id)
	var bodyview: BodyView = null
	for node in map.get_children():
		if node is BodyView:
			if node.entity_id == entity.id:
				bodyview = node
				break
	if bodyview:
		bodyview.get_sprite().animate(request.fx)

func _on_exit(_request: Request):
	TransitionManager.begin_transition()
	yield(TransitionManager, "screen_dimmed")
	pop()
	TransitionManager.end_transition()

func preload_entities():
	for spawn in map.get_children():
		if spawn is EntitySpawn:
			var effects := spawn._spawn(game.simulation) as Array
			spawn.queue_free()
			for effect in effects:
				effect = RuleSolver.resolve(game.simulation, effect)
				var spawn_trait := effect.find_trait(Spawn) as Spawn
				var view: BodyView = Constants.body_view_scene.instance()
				if view.load_from_spawn(spawn_trait):
					map.add_child(view)
					view.snap_to_grid(map.get_hex_grid())

func _update_bodyview_positions():
	for bodyview in map.get_children():
		if bodyview is BodyView:
			bodyview.hex = EntityAccess.get_body_hex(game.simulation,
													 bodyview.entity_id)
