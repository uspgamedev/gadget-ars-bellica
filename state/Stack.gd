class_name Stack extends Object

signal state_changed(state)

var stack := []
var locked := false

func empty() -> bool:
	return stack.empty()

func clear():
	assert(not locked)
	stack.clear()

func current() -> State:
	return stack.back() as State

func push(state: State, game: Game):
	assert(not locked)
	var previous: State = null
	if !stack.empty():
		previous = current()
		locked = true
		previous._suspend()
		locked = false
	stack.push_back(state)
	prepare_state(state, game)
	current()._enter(previous)
	emit_signal("state_changed", current())

func pop():
	assert(not locked)
	locked = true
	current()._leave()
	locked = false
	var previous := current()
	stack.pop_back()
	if !stack.empty():
		current()._resume(previous)
		emit_signal("state_changed", current())
	previous.call_deferred("free")

func switch(state: State, game: Game):
	assert(not locked)
	if stack.empty():
		return
	locked = true
	current()._leave()
	locked = false
	var previous := current()
	stack.pop_back()
	stack.push_back(state)
	prepare_state(state, game)
	current()._enter(previous)
	emit_signal("state_changed", current())

func prepare_state(state: State, game: Game):
	state.game = game
	#warning-ignore:return_value_discarded
	state.connect("state_pushed", self, "push", [game])
	#warning-ignore:return_value_discarded
	state.connect("state_switched", self, "switch", [game])
	#warning-ignore:return_value_discarded
	state.connect("state_popped", self, "pop")
