class_name State extends Object

signal state_pushed(state)
signal state_popped()
signal state_switched(state)

var game: Game = null

func _enter(_previous: State):
	pass # Abstract method

func _suspend():
	pass # Abstract method

func _resume(_previous: State):
	pass # Abstract method

func _leave():
	pass # Abstract method

func push(state: State):
	emit_signal("state_pushed", state)

func pop():
	emit_signal("state_popped")

func switch(state: State):
	emit_signal("state_switched", state)

func is_state(script: Script) -> bool:
	return get_script() == script
