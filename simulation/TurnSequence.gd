class_name TurnOrder extends Node

var order := []
var current := 0

func get_current_turn () -> int:
	return order[current]

func advance_turn ():
	current = (current + 1) % order.size()
