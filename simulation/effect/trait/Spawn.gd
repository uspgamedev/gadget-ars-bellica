class_name Spawn extends Trait

export var entity_scn: PackedScene
export var hex: Vector2

var entity: Entity = null

static func get_typename() -> String:
	return "spawn"

func _init(the_entity_scn: PackedScene, the_hex: Vector2):
	entity_scn = the_entity_scn
	hex = the_hex
