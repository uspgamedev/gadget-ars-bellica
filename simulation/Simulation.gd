class_name Simulation extends Node

onready var entities := {}
onready var next_id := 0

onready var blocked_tiles := {}

func get_turn_order() -> TurnOrder:
	return $TurnOrder as TurnOrder

func find_entity(id: int) -> Entity:
	return entities.get(id, null)

func find_entities_with_property(type: Script) -> Array:
	var entities_with_property := []
	for entity in entities.values():
		if entity.find_property(type):
			entities_with_property.append(entity)
	return entities_with_property

func register_entity(entity: Entity):
	var id := next_id
	next_id += 1
	entities[id] = entity
	entity.id = id
	$ActiveEntities.add_child(entity)

func block_tile(hex: Vector2):
	blocked_tiles[hex] = true

func unblock_tile(hex: Vector2):
	return blocked_tiles.erase(hex)

func is_tile_blocked(hex: Vector2) -> bool:
	return blocked_tiles.has(hex)
