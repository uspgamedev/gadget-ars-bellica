extends Actor

func get_next_action(simulation: Simulation) -> Action:
	var hex := simulation.find_entity(entity_id).find_property(Body).hex as Vector2
	var targets := simulation.find_entities_with_property(Ally)
	for target in targets:
		if target is Entity:
			var target_hex := target.find_property(Body).hex as Vector2
			var dist := Hex.distance(hex, target_hex)
			if dist == 1:
				return UseSkillAction.new(target_hex)
	var dir: int 
	while true:
		dir = randi() % Hex.DIR2HEX.size()
		var motion := Hex.DIR2HEX[dir] as Vector2
		var new_hex = hex + motion
		if not ActionProtocol.is_tile_blocked_for(simulation, entity_id, new_hex):
			break
	return WalkAction.new(dir)
