tool
class_name Entity extends Node

var id := Definitions.ENTITY_INVALID_ID

func _ready():
	if !Engine.editor_hint:
		for property in get_children():
			if property is Property:
				property.entity_id = id

func find_property(type: Script) -> Node:
	for child in get_children():
		if child is type:
#		if child.get_script().instance_has(type):
			return child as Node
	return null

func has_properties(properties: Array):
	for property in properties:
		if find_property(property) == null:
			return false
	return true
