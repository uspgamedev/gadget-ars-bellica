class_name Damageable extends Property

export var max_hp := 10

onready var hp := max_hp setget set_hp
onready var requester := Requester.new()

func _ready():
	add_child(requester)

func set_hp(value: int):
	var old_hp = hp
	hp = max(0, min(max_hp, value) as int) as int
	var request := EntityFXRequest.new()
	request.entity_id = entity_id
	if hp <= old_hp:
		request.fx = 'damage'
	elif hp > old_hp:
		request.fx = 'heal'
	requester.request('entity_state_changed', request)
