class_name WalkAction extends Action

var motion: Vector2

func _init(direction: int):
	motion = Hex.DIR2HEX[direction]

func execute(simulation: Simulation, entity_id: int) -> Effect:
	var entity := simulation.find_entity(entity_id)
	var body := entity.find_property(Body) as Body
	var to_hex := body.hex + motion
	return Effect.new().with_trait(Move.new(entity_id, to_hex))
