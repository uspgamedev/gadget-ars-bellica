class_name UseSkillAction extends Action

var target_hex: Vector2

func _init(hex: Vector2):
	target_hex = hex

func execute(simulation: Simulation, _entity_id: int) -> Effect:
	var target: Entity = null
	for other_id in simulation.entities:
		var entity := simulation.find_entity(other_id)
		var body := entity.find_property(Body) as Body
		if body != null and body.hex == target_hex:
			target = entity
			break
	if target != null:
		return Effect.new() \
			.with_trait(Damage.new(3)) \
			.with_trait(Target.new([target.id]))
	else:
		return null
