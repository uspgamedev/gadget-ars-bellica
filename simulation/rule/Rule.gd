class_name Rule extends Node

const DIRTY_RULE_GROUP = "dirty_rule_group"
const TRAIT_PROCESSING_GROUP = "trait(%s):process"
const TRAIT_APPLICATION_GROUP = "trait(%s):apply"

export(Array, Script) var EFFECT_TRAITS
export var PROCESSES_EFFECT := false
export var APPLIES_EFFECT := false

var entity_id: int
var processed_effects := {}
var applied_effects := {}

static func group_for_trait_processing(trait_type: Script) -> String:
	return TRAIT_PROCESSING_GROUP % trait_type.get_typename()

static func group_for_trait_application(trait_type: Script) -> String:
	return TRAIT_APPLICATION_GROUP % trait_type.get_typename()

func _ready():
	for trait in EFFECT_TRAITS:
		if PROCESSES_EFFECT:
			add_to_group(group_for_trait_processing(trait))
		if APPLIES_EFFECT:
			add_to_group(group_for_trait_application(trait))

func accepts(effect: Effect) -> bool:
	for type in EFFECT_TRAITS:
		if effect.find_trait(type) == null:
			return false
	return true

func process_effect(simulation: Simulation, effect: Effect) -> Effect:
	if accepts(effect) and not processed_effects.has(effect):
		processed_effects[effect] = true
		add_to_group(DIRTY_RULE_GROUP)
		return _process_effect(simulation, effect)
	else:
		return effect

func _process_effect(_simulation: Simulation, effect: Effect) -> Effect:
	return effect

func apply_effect(simulation: Simulation, effect: Effect):
	if accepts(effect) and not applied_effects.has(effect):
		applied_effects[effect] = true
		add_to_group(DIRTY_RULE_GROUP)
		_apply_effect(simulation, effect)

func _apply_effect(_simulation: Simulation, _effect: Effect):
	pass

static func clear_all_rules(tree: SceneTree):
	for rule in tree.get_nodes_in_group(DIRTY_RULE_GROUP):
		rule.processed_effects.clear()
		rule.applied_effects.clear()
		(rule as Node).remove_from_group(DIRTY_RULE_GROUP)
