extends Rule

func _process_effect(simulation: Simulation, effect: Effect) -> Effect:
	var move := effect.find_trait(Move) as Move
	var entity := simulation.find_entity(move.moved_entity_id)
	if entity.has_properties([Body, Movable]):
		return effect
	else:
		return effect.prevent()

func _apply_effect(simulation: Simulation, effect: Effect):
	var move := effect.find_trait(Move) as Move
	var entity := simulation.find_entity(move.moved_entity_id)
	var body := entity.find_property(Body) as Body
	ActionProtocol.update_blocked_tiles(simulation, entity.id, body.hex,
										move.to_hex)
	body.hex = move.to_hex
