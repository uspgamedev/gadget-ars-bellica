extends Rule

func _apply_effect(simulation: Simulation, effect: Effect):
	var damage := effect.find_trait(Damage) as Damage
	var target := effect.find_trait(Target) as Target
	if target == null:
		return
	
	for id in target.target_ids:
		var entity := simulation.find_entity(id)
		var damageable := entity.find_property(Damageable) as Damageable
		damageable.hp -= damage.amount
