extends Rule

func _apply_effect(simulation: Simulation, effect: Effect):
	var spawn := effect.find_trait(Spawn) as Spawn
	var turn := spawn.entity.find_property(Turn) as Turn
	if turn != null:
		simulation.get_turn_order().order.append(spawn.entity.id)
