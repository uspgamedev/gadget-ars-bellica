class_name ActionProtocol extends Object

static func advance_turns(simulation: Simulation):
	var turn_order := simulation.get_turn_order()
	while true:
		var id := turn_order.get_current_turn()
		var entity := simulation.find_entity(id)
		var actor: Actor = entity.find_property(Actor);
		if actor:
			var action := actor.get_next_action(simulation)
			var effect := action.execute(simulation, id) if action else null
			if effect:
				#warning-ignore:return_value_discarded
				RuleSolver.resolve(simulation, effect)
			else:
				break
		turn_order.advance_turn()

static func commit_movement(simulation: Simulation, id: int, new_pos: Vector2):
	var old_hex := EntityAccess.get_body_hex(simulation, id)
	var dir := Hex.closest_dir(new_pos - old_hex)
	EntityAccess.set_player_action(simulation, id, WalkAction.new(dir))

static func try_attack(simulation: Simulation, id: int, target_hex: Vector2):
	EntityAccess.set_player_action(simulation, id , UseSkillAction.new(target_hex))

static func update_blocked_tiles(simulation: Simulation, entity_id: int,
								 from_hex: Vector2, to_hex: Vector2):
	var entity := simulation.find_entity(entity_id)
	if entity != null and entity.find_property(Blocks) != null:
		simulation.unblock_tile(from_hex)
		simulation.block_tile(to_hex)

static func is_tile_blocked_for(simulation: Simulation, entity_id: int,
								hex: Vector2):
	var entity := simulation.find_entity(entity_id)
	return entity.find_property(Blocks) != null \
	   and simulation.is_tile_blocked(hex)
