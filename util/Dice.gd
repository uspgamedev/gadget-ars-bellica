class_name Dice extends Object

static func roll(times, sides) -> int:
	var sum := 0
	for n in times:
		sum += randi() % sides + 1
	return sum
