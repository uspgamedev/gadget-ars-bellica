extends Node

export var title_menu_state: Script
export var campaign_state: Script
export var turn_tracking_state: Script
export var gameplay_mode_state : Script
export var exploration_mode_state: Script
export var tactical_mode_state: Script

export var player_entity_scene: PackedScene

export var map_scene: PackedScene
export var player_view_scene: PackedScene
export var body_view_scene: PackedScene
