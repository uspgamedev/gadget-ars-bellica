class_name AutoConnection extends Reference

var emitter_type: Script
var signal_name: String

var target_obj: Object
var method_name: String

func _init(type: Script, the_signal: String, target: Object, method: String):
	emitter_type = type
	target_obj = target
	signal_name = the_signal
	method_name = method

func traverse_and_connect(node: Node):
	_connect_signal(node)
	for child in node.get_children():
		traverse_and_connect(child)

func watch(tree: SceneTree):
	SignalHelper.safe_connect(tree, "node_added", self, "_connect_signal")

func _connect_signal(node):
	if node is emitter_type:
		SignalHelper.safe_connect(node, signal_name, target_obj, method_name)
