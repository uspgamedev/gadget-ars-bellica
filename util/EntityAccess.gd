class_name EntityAccess extends Object

static func get_body_hex(simulation: Simulation, entity_id: int) -> Vector2:
	var entity := simulation.find_entity(entity_id)
	var body := entity.find_property(Body)
	if body:
		return body.hex
	else:
		return Vector2.ZERO # Is there a better choice?

static func set_player_action(simulation: Simulation, id: int, action: Action):
	var entity := simulation.find_entity(id)
	var actor := entity.find_property(Actor) as PlayerActor
	if actor:
		actor.set_next_action(action)
