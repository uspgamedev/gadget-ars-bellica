tool
class_name AnimatedBillboardSprite3D extends AnimatedSprite3D

const IDLE_ANIMATION := "default"
const MOVING_ANIMATION := "moving"

onready var shader := material_override as ShaderMaterial

func set_idle_animation():
	animation = IDLE_ANIMATION

func set_moving_animation():
	if frames.has_animation(MOVING_ANIMATION):
		animation = MOVING_ANIMATION
	else:
		set_idle_animation()

func animate(anim_name: String):
	$AnimationPlayer.play(anim_name)

func _ready():
	if frames != null:
		shader.set_shader_param("scale", scale)

func _process(_delta):
	if frames != null:
		var texture := frames.get_frame(animation, frame)
		shader.set_shader_param("sprite", texture)
		offset.x = -texture.get_width() / 2
	if Engine.editor_hint:
		set_process(false) # reduce in-editor overload
