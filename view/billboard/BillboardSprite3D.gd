tool
class_name BillboardSprite3D extends Sprite3D

func _ready():
	if texture != null:
		var shader := material_override as ShaderMaterial
		shader.set_shader_param("sprite", texture)
		shader.set_shader_param("scale", scale)
		offset.x = -texture.get_width() / 2
