tool
class_name HexGrid extends Spatial

class Cell:
	extends Spatial
	var q: int
	var r: int
	var mesh: Spatial
	func set_mesh(scn: PackedScene):
		if mesh != null:
			remove_child(mesh)
			mesh = null
		if scn != null:
			mesh = scn.instance()
			add_child(mesh)
	func get_hex() -> Vector2:
		return Vector2(q, r)

const STEP := sqrt(3)
const RADIUS := 10

const Q_AXIS := Vector3(2 * STEP, 0, 0)
const R_AXIS := Vector3(STEP, 0, 3)

export var default_cellmesh_scn: PackedScene
export var unit := 0.5 setget set_unit

var cells := []

var q_max: int
var r_max: int

func _ready():
	_generate_cells()

func set_unit(value):
	unit = value
	for cell in cells:
		if cell is Cell:
			cell.translation = qr2position(cell.q, cell.r)

func _generate_cells():
	cells.clear()
	for child in get_children():
		if child is Cell:
			remove_child(child)
	q_max = 2*RADIUS + 1
	r_max = 2*RADIUS + 1
	for r in r_max:
		for q in q_max:
			var start := row_start(r)
			if q >= start and q < start + row_size(r):
				var cell = Cell.new()
				cell.q = q
				cell.r = r
				cell.translation = qr2position(q, r)
				cell.set_mesh(default_cellmesh_scn)
				add_child(cell)
				cells.append(cell)

func qr2position(q: int, r: int) -> Vector3:
	return get_origin() + unit * (Q_AXIS * q + R_AXIS * r)

func get_origin() -> Vector3:
	return RADIUS * unit * -(R_AXIS + Q_AXIS)

func row_start(r: int) -> int:
	return max(0, RADIUS - r) as int

func row_size(r: int) -> int:
	return r_max - abs(RADIUS - r) as int

func get_cell(q: int, r: int) -> Cell:
	var index := qr2index(q, r)
	if index >= 0 and index < get_child_count():
		return get_child(index) as Cell
	else:
		return null

func get_cellv(hex: Vector2) -> Cell:
	return get_cell(hex.x as int, hex.y as int)

func qr2index(q: int, r: int) -> int:
	var offset := 0
	for i in r:
		offset += row_size(i)
	var j := q - max(0, RADIUS - r) as int
	return offset + j

func total() -> int:
	return qr2index(0, r_max + 1)

func find_closest_cell(global_pos: Vector3) -> Cell:
	var local_pos := to_local(global_pos) #+ R_AXIS * unit
	var mindist := INF
	var picked: Cell = null
	for cell in cells:
		if cell is Cell:
			var dist := cell.translation.distance_to(local_pos) as float
			if dist < mindist:
				mindist = dist
				picked = cell
	return picked

func find_second_closest_cell(global_pos: Vector3) -> Cell:
	var local_pos := to_local(global_pos) #+ R_AXIS * unit
	var mindist := INF
	var smindist := INF
	var picked: Cell = null
	var spicked: Cell = null
	for cell in cells:
		if cell is Cell:
			var dist := cell.translation.distance_to(local_pos) as float
			if dist < mindist:
				smindist = mindist
				spicked = picked
				mindist = dist
				picked = cell
			elif dist < smindist:
				smindist = dist
				spicked = cell
	return spicked
