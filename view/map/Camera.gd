class_name MapCamera extends Camera

export var CAM_SPEED := .2

var attached_view_path : NodePath
var dragging := false
var drag_start_x := 0.0
var drag_start_angle := 0.0
var drag_target_angle := 0.0

func attach(view: BodyView):
	view.get_camera_hook().remote_path = get_path()
	attached_view_path = view.get_path()

func _input(event):
	var view := get_node_or_null(attached_view_path) as BodyView
	if view != null:
		if event is InputEventMouseButton:
			if event.is_pressed() and event.button_index == BUTTON_RIGHT:
				dragging = true
				drag_start_angle = view.rotation.y
				drag_start_x = event.position.x
				drag_target_angle = drag_start_angle
			elif not event.is_pressed() and event.button_index == BUTTON_RIGHT:
				dragging = false

func _process(delta):
	var view := get_node_or_null(attached_view_path) as BodyView
	if view != null:
		if dragging:
			var mouse_pos := get_viewport().get_mouse_position()
			var diff_x := mouse_pos.x - drag_start_x
			var diff_angle := -deg2rad(diff_x * CAM_SPEED)
			drag_target_angle = drag_start_angle + diff_angle
		var diff = drag_target_angle - view.rotation.y
		if diff > PI:
			diff -= 2*PI
		elif diff < -PI:
			diff += 2*PI
		view.rotate_y(diff * 5 * delta)
