class_name BodyViewBase extends KinematicBody

const BODY_VIEW_GROUP = "body view"

export(float, 1, 10) var smooth_factor := 2.0
export var hex := Vector2.ZERO

func _ready():
	add_to_group(BODY_VIEW_GROUP)
	$Sprite.playing = true

func _process_position(map: HexGrid, delta: float):
	var target_pos := map.qr2position(hex.x as int, hex.y as int)
	translate((target_pos - translation) * smooth_factor * delta)

func snap_to_grid(grid: HexGrid):
	global_transform.origin = grid.qr2position(hex.x as int, hex.y as int)

func get_sprite() -> AnimatedSprite3D:
	return $Sprite as AnimatedSprite3D

func get_camera_hook() -> RemoteTransform:
	return $CameraHook as RemoteTransform
