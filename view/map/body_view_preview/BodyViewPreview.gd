tool
class_name BodyViewPreview extends BodyViewBase


var entity : Entity = null

func _ready() -> void:
	if !entity: return
	var body := entity.find_property(Body) as Body
	if body:
		$Sprite.frames = body.view
		$Sprite.offset = body.sprite_offset
		$Sprite.animation = body.view.animations[0].name
		if body.view.animations.size() > 1:
			for i in body.view.animations.size():
				if i == 0: continue
				var sprite := body.sprite_scn.instance() as AnimatedBillboardSprite3D
				sprite.frames = body.view
				sprite.offset = body.sprite_offset
				sprite.animation = body.view.animations[i].name
				self.add_child(sprite)
				sprite.transform.origin.z = -1 * i
				sprite.playing = true
	else: self.queue_free()
