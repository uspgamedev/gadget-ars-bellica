class_name BodyView extends BodyViewBase

var entity_id := Definitions.ENTITY_INVALID_ID;

func load_from_spawn(spawn: Spawn):
	if spawn == null: return false
	var body := spawn.entity.find_property(Body) as Body
	if body == null: return false
	entity_id = spawn.entity.id
	hex = spawn.hex
	$Sprite.frames = body.view
	$Sprite.offset = body.sprite_offset
	return true

func snap_to_entity(simulation: Simulation): 
	hex = simulation.find_entity(entity_id).find_property(Body).hex
