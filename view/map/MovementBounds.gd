tool
extends StaticBody

export var enabled := false setget set_enabled

func set_enabled(value):
	enabled = value
	for shape in get_children():
		if shape is CollisionShape:
			shape.disabled = not value
