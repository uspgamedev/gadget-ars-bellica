tool
class_name SingleEntitySpawn extends EntitySpawn

export var entity_scn: PackedScene setget set_entity_scn
export var sprite_scn: PackedScene

onready var sprite: AnimatedSprite3D = null

func _ready():
	refresh_preview()

func _get_configuration_warning():
	if entity_scn == null:
		return "Missing Entity scene."
	else:
		return ""

func _spawn(_simulation: Simulation) -> Array:
	return [create(entity_scn, hex)]

func refresh_preview():
	if entity_scn != null and sprite_scn != null:
		var preview := entity_scn.instance() as Entity
		var body := preview.find_property(Body) as Body
		if body != null:
			if sprite != null:
				for child in get_children():
					remove_child(child)
				sprite.free()
			sprite = sprite_scn.instance()
			sprite.frames = body.view.duplicate()
			sprite.offset = body.sprite_offset
			add_child(sprite)
		preview.free()

func set_entity_scn(scn: PackedScene):
	entity_scn = scn
	refresh_preview()
