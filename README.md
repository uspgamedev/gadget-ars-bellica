
# GRIMOIRE: Ars Bellica

A high fantasy, turn-based, sandbox, tactical role-playing game!

## How to run the game

Clone the repository and open the `project.godot` file using the latest stable
version of [Godot](https://godotengine.org/).

