class_name AbilityHelper extends Object

static func find_ability(node: Node) -> Ability:
	for child in node.get_children():
		if child is Ability:
			return child
	return null
