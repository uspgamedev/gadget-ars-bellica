class_name ChooseAction extends State

export var WALK_ABILITY: PackedScene
export var PASS_ABILITY: PackedScene
export var ERASE_SPELL_ABILITY: PackedScene
export var REALTIME := false

onready var current_destination := Vector2()
onready var auto_travel := false
onready var available_abilities := {}
onready var default_special_action = null
onready var walk_ability := WALK_ABILITY.instance() as Ability
onready var pass_ability := PASS_ABILITY.instance() as Ability
onready var erase_spell_ability := ERASE_SPELL_ABILITY.instance() as Ability
onready var auto_save := false

signal confirmed(flag)

func _ready():
	call_deferred("add_child", walk_ability)
	call_deferred("add_child", pass_ability)
	call_deferred("add_child", erase_spell_ability)

func _enter(_previous: State):
	setup_timer()
	if auto_travel:
		_on_map_tile_clicked(MapTileClickedRequest.new(current_destination))
	available_abilities.clear()
	default_special_action = null
	for property in battle.get_map().get_player().get_children():
		if property is Property:
			var abilities = property._available_abilities(battle.get_map())
			for ability_data in abilities:
				available_abilities[ability_data.name] = {
					ability = ability_data.ability,
					data = ability_data.data
				}
				if default_special_action == null:
					default_special_action = ability_data
	
	auto_save = true

func _leave():
	battle.get_map().get_player().get_property(Player).show_dir_hint = false

func _resume(previous: State):
	if previous is PrepareSpell and previous.use != null:
		var prepare_spell := previous as PrepareSpell
		var player := battle.get_map().get_player()
		player.get_property(Actor).next_action = prepare_spell.use
		pop()
	elif previous is Confirm:
		emit_signal("confirmed", previous.confirmed)
	else:
		setup_timer()


func setup_timer():
	if REALTIME:
		#warning-ignore:return_value_discarded
		$Timer.connect("timeout", self, "_on_timeout")
		$Timer.start()

func _on_process(_request: ControlRequest):
	if not battle.get_map().has_entities_moving():
		battle.get_ui().set_primary_action_hint("MOVE")
		battle.get_ui().set_secondary_action_hint("WAIT")
		if default_special_action == null:
			battle.get_ui().set_special_action_hint("---")
		else:
			var action := default_special_action["name"] as String
			battle.get_ui().set_special_action_hint(action.to_upper())
		battle.get_map().get_player().get_property(Player).show_dir_hint = true
	if auto_save and not battle.get_map().has_entities_moving():
		battle.request_save()
		auto_save = false

func _on_timeout():
	var player := battle.get_map().get_player()
	var skip := AbilityUse.new(pass_ability, battle.get_map(), player)
	player.get_property(Actor).next_action = skip
	pop()

func _suspend():
	if REALTIME:
		$Timer.stop()
		$Timer.disconnect("timeout", self, "_on_timeout")
	battle.get_map().get_player().get_property(Player).show_dir_hint = false

func _on_map_tile_clicked(request: MapTileClickedRequest):
	var player: Entity = battle.get_map().get_player()
	var actor := player.get_property(Actor) as Actor
	var tile: Vector2 = request.tile
	if not auto_travel and battle.is_map_safe():
		current_destination = tile
		auto_travel = true
	var player_tile := player.tile
	if auto_travel and player_tile == current_destination:
		auto_travel = false
		return
	var diff = tile - player_tile
	var dir = Tile.to_closest_dir(diff)
	var target_tile = player_tile + Tile.FROM_DIR[dir]
	var walk := AbilityUse.new(walk_ability, battle.get_map(), player) \
		.with_target_tile(target_tile)
	var pending = walk.pending_command()
	if pending == null:
		actor.next_action = walk
		pop()
	else:
		auto_travel = false

func _on_cancel(_request: ControlRequest):
	var player := battle.get_map().get_player()
	var actor := player.get_property(Actor) as Actor
	var wait := AbilityUse.new(actor.wait, battle.get_map(), player)
	var pending = wait.pending_command()
	if pending == null:
		actor.next_action = wait
		pop()
	else:
		auto_travel = false

func _on_spell_selected(request: SpellSelectedRequest):
	var state := get_state(PrepareSpell) as PrepareSpell
	state.set_spell(request.spell).push()

func _on_spell_erased(request: SpellSelectedRequest):
	SignalHelper.reconnect(self, "confirmed", self, "erase_spell", \
						   [request.spell], CONNECT_ONESHOT)
	var confirm_state := get_state(Confirm) as Confirm
	confirm_state.query = "Erase spell from grimoire?"
	confirm_state.push()

func erase_spell(flag: bool, spell: Spell):
	if not flag: return
	var player: Entity = battle.get_map().get_player()
	var actor := player.get_property(Actor) as Actor
	var ability := AbilityUse \
		.new(erase_spell_ability, battle.get_map(), player) \
		.with_target_spell(spell.get_path())
	if ability.pending_command() == null:
		actor.next_action = ability
		pop()

func _on_action_selected(request: ActionSelectedRequest):
	if not request.action_name in available_abilities.keys(): return
	var player: Entity = battle.get_map().get_player()
	var actor := player.get_property(Actor) as Actor
	var ability_info := available_abilities[request.action_name] as Dictionary
	var ability := AbilityUse.new(ability_info.ability, battle.get_map(), \
								  player) \
		.with(ability_info.data)
	if ability.pending_command() == null:
		actor.next_action = ability
		pop()

func _on_interact(_request: ControlRequest):
	if default_special_action == null: return
	var player: Entity = battle.get_map().get_player()
	var actor := player.get_property(Actor) as Actor
	var ability := default_special_action.ability as Ability
	var use := AbilityUse.new(ability, battle.get_map(), \
							  player) \
		.with(default_special_action.data)
	if use.pending_command() == null:
		actor.next_action = use
		pop()

func get_hint() -> String:
	return "Move towards here"
