class_name ResolveAbility extends State

# Whenever an ability happens, other abilities might trigger. Triggered
# abilities are queued after the original ability, with further triggered
# abilities doing the same. Some triggered abilities, however, change the
# original ability, so they must happen first.

onready var ability_queue := []
onready var frames := 0
onready var current_ability: AbilityUse = null

func _ready():
	#warning-ignore:return_value_discarded
	get_tree().connect("node_added", self, "_register_trigger")

func _register_trigger(trigger_rule):
	if trigger_rule is TriggerRule:
		SignalHelper.safe_connect(trigger_rule, "ability_triggered", \
								  self, "queue_ability")

func queue_ability(ability: AbilityUse) -> ResolveAbility:
	if is_inside_tree():
		if ability.map == null:
			ability.map = battle.get_map()
		ability_queue.append(ability)
	return self

func _enter(_previous: State):
	for trigger_rule in get_tree().get_nodes_in_group(TriggerRule.GROUP_NAME):
		_register_trigger(trigger_rule)
	resolve_abilities()

func _on_process(_request: ControlRequest):
	frames += 1

# Resolve at most one ability per frame, letting animations finish before
# continuing.
func resolve_abilities():
	while not ability_queue.empty() and current_ability == null:
		# There are pending abilities, resolve them
		current_ability = ability_queue.pop_front() as AbilityUse
		var result = current_ability.perform()
		while result is GDScriptFunctionState:
			result = yield(result, "completed")
		if not current_ability.actor_entity.is_inside_tree():
			return
		current_ability = null
	# No more abilities to resolve, this state is done
	pop()

