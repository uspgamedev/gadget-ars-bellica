extends TileMap

export var SIZE := 22
export var CURSOR_SMOOTH := 15

onready var cursor_tile := Vector2.ZERO

func screen_to_tile(screen_position: Vector2) -> Vector2:
	return world_to_map((screen_position - self.global_position) / self.global_scale)

func _process(delta):
	self.cursor_tile = screen_to_tile(get_global_mouse_position())
	self.cursor_tile.x = clamp(self.cursor_tile.x, 0, SIZE - 1)
	self.cursor_tile.y = clamp(self.cursor_tile.y, 0, SIZE - 1)
	var cursor_pos = map_to_world(self.cursor_tile)
	$Cursor.position += (cursor_pos - $Cursor.position) * CURSOR_SMOOTH * delta

func highlight(tiles: Array):
	$Highlight.clear()
	for tile in tiles:
		$Highlight.set_cellv(tile, $Highlight.tile_set.find_tile_by_name("blank"))
