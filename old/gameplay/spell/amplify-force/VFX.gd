extends AnimatedSprite

export var A := 1000

onready var center := Tile.center_offset()
onready var motion := Vector2(10, 0)

func _ready():
	play()

func _process(delta):
	var diff := center - position
	if diff.length_squared() > 1:
		motion += A * delta * diff / diff.length_squared()
	position += motion * delta
