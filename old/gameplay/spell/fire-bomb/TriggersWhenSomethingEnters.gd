extends TriggerRule

func _apply_effect(map: Map, effect: Effect):
	if effect is SenseEffect:
		if		effect.data.has_flag(AreaSensor.OTHER_ENTERED) \
			or	effect.data.has_flag(AreaSensor.SELF_ENTERED):
				trigger_ability(map)
	elif effect is EnergyEffect:
		var energy_effect := effect as EnergyEffect
		if		energy_effect.has_element(Element.TYPE.IMPACT) \
			or	energy_effect.has_element(Element.TYPE.FIRE):
			trigger_ability(map)
