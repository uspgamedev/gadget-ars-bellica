tool
class_name OnTarget extends Command

enum TargetField {
	ENTITY_ON_TARGET_TILE,
	ENTITY_THAT_TRIGGERED_ABILITY,
	ENTITY_TARGET
}

export(TargetField) var TARGET = TargetField.ENTITY_ON_TARGET_TILE
export(Resource) var movement_profile
export var hit_msg := "The %s hits the %s!"
export var miss_msg := "The %s misses..."
export var VFX_SCN: PackedScene

signal message(text)

func _ready():
	if movement_profile == null:
		movement_profile = MovementProfile.new()

func perform(use: AbilityUse):
	var source_entity := use.actor_entity
	var target_entity := find_entity(use)
	if VFX_SCN != null:
		var vfx := VFX_SCN.instance()
		var tile: Vector2
		match TARGET:
			TargetField.ENTITY_ON_TARGET_TILE:
				tile = source_entity.tile + use.target.target_offset
				if vfx.has_method("set_source_path"):
					vfx.set_source_path(source_entity.get_path())
			TargetField.ENTITY_THAT_TRIGGERED_ABILITY:
				tile = target_entity.tile
			TargetField.ENTITY_TARGET:
				tile = target_entity.tile
		if tile != null:
			use.map.add_vfx_at(vfx, tile)
			if vfx.has_signal("hit"):
				yield(vfx, "hit")
	if target_entity != null:
		emit_signal("message", hit_msg % [source_entity.entity_name, \
										  target_entity.entity_name])
		for cmd in get_children():
			var sub_effect := (cmd as Command).evaluate(use) as Effect
			var effect := TargetEffect.new()\
				.target(target_entity)\
				.cause(sub_effect)\
				.with_profile(movement_profile)\
				.on(source_entity)
			effect = EffectSolver.resolve(use.map, effect)
			if effect.is_succesfull():
				success()
	if not successful:
		emit_signal("message", miss_msg % source_entity.entity_name)

func find_entity(use: AbilityUse) -> Entity:
	var entity := use.actor_entity
	var map := use.map
	var target := use.target
	match TARGET:
		TargetField.ENTITY_ON_TARGET_TILE:
			return map.find_entity_at(entity.tile + target.target_offset)
		TargetField.ENTITY_THAT_TRIGGERED_ABILITY:
			var path := target.triggering_entity_path as NodePath
			return get_node_or_null(path) as Entity
		TargetField.ENTITY_TARGET:
			var path := target.target_entity_path as NodePath
			return get_node_or_null(path) as Entity
	return null
