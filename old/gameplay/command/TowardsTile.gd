extends Command

func evaluate(use: AbilityUse):
	if get_child_count() < 2: return
	var origin_tile := get_child(0).evaluate(use) as Vector2
	var target_tile := get_child(1).evaluate(use) as Vector2
	if origin_tile == target_tile:
		return origin_tile
	else:
		var dir := Tile.to_closest_dir(target_tile - origin_tile)
		var offset := Tile.FROM_DIR[dir] as Vector2
		return origin_tile + offset
