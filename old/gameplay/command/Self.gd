extends Command

class_name Self

func evaluate(use: AbilityUse):
	return use.actor_entity.get_path()
