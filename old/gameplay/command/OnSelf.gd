class_name OnSelf extends Command

export var VFX_SCN: PackedScene

func perform(use: AbilityUse):
	if get_child_count() < 1: return
	for cmd in get_children():
		if cmd is NewEffect:
			var effect := cmd.evaluate(use) as Effect
			if effect.data.tile != Tile.INVALID and VFX_SCN != null:
				var vfx := VFX_SCN.instance()
				if vfx.has_method("set_source_path"):
					vfx.set_source_path(use.actor_entity.get_path())
				use.map.add_vfx_at(vfx, effect.data.tile)
				if vfx.has_signal("hit"):
					yield(vfx, "hit")
			effect = effect.on(use.actor_entity)
			if not EffectSolver.resolve(use.map, effect).is_succesfull():
				return
	success()
