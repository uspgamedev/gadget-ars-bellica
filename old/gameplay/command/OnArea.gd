tool
class_name OnArea extends Command

enum TargetField {
	SELF_TILE,
	TARGET_TILE,
	EVAL_TILE
}

export(TargetField)	var TARGET = TargetField.SELF_TILE
export(int, 0, 20)	var RADIUS = 0
export(NodePath)	var EVAL_PATH
export(Resource)	var movement_profile
export var VFX_SCN: PackedScene

func _ready():
	if movement_profile == null:
		movement_profile = MovementProfile.new()

func perform(use: AbilityUse):
	var source_entity := use.actor_entity
	var target_tile: Vector2
	match TARGET:
		TargetField.SELF_TILE:
			target_tile = source_entity.tile
		TargetField.TARGET_TILE:
			target_tile = source_entity.tile + use.target.target_offset
		TargetField.EVAL_TILE:
			var cmd := get_node(EVAL_PATH) as Command
			target_tile = cmd.evaluate(use) as Vector2
	for cmd in get_children():
		var sub_effect := (cmd as Command).evaluate(use) as Effect
		var effect := AreaEffect.new()\
			.on_tile(target_tile)\
			.with_radius(RADIUS)\
			.with_profile(movement_profile)\
			.cause(sub_effect)\
			.on(source_entity)
		if EffectSolver.resolve(use.map, effect).is_succesfull():
			success()
	if VFX_SCN != null:
		var tile_range = range(-RADIUS, RADIUS+1)
		for y in tile_range:
			for x in tile_range:
				var offset := Vector2(x, y)
				var tile := target_tile + offset
				use.map.add_vfx_at(VFX_SCN.instance(), tile)
