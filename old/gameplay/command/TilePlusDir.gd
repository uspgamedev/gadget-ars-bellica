extends Command

class_name TilePlusDir

export var MULTIPLIER := 1

func evaluate(use: AbilityUse):
	if get_child_count() < 2: return
	var tile := get_child(0).evaluate(use) as Vector2
	var dir = get_child(1).evaluate(use)
	var offset: Vector2
	if dir is int:
		offset = Tile.FROM_DIR[dir]
	elif dir is Vector2:
		offset = dir
	return tile + offset * MULTIPLIER
