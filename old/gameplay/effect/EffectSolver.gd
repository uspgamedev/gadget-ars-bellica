class_name EffectSolver extends Object

static func resolve(map: Map, effect: Effect) -> Effect:
	var current_effect := effect
	while current_effect != null:
		var entity := current_effect.affected_entity
		if		entity != null \
			and not entity.is_queued_for_deletion() \
			and not current_effect.is_prevented():
				var old_effect := current_effect
				current_effect = process_effect(map, entity, current_effect)
				if old_effect != current_effect:
					continue # effect changed, restart processing
				if current_effect.is_succesfull():
					apply_effect(map, entity, current_effect)
		current_effect = current_effect.next
	Rule.clear_all_rules(map.get_tree())
	return effect

static func preview(map: Map, effect: Effect) -> Effect:
	var current_effect := effect
	effect.data.set_flag(EffectData.PREVIEW_FLAG)
	while current_effect != null:
		var entity := current_effect.affected_entity
		if		entity != null \
			and not entity.is_queued_for_deletion() \
			and not current_effect.is_prevented():
				var old_effect := current_effect
				current_effect = process_effect(map, entity, current_effect)
				if old_effect != current_effect:
					continue # effect changed, restart processing
		current_effect = current_effect.next
	Rule.clear_all_rules(map.get_tree())
	return effect

static func process_effect(map: Map, entity: Entity, effect: Effect) -> Effect:
	for property in entity.get_children():
		if property is Property:
			for rule in property.get_children():
				if rule is Rule:
					if rule.PROCESSES_EFFECT:
						effect = rule.process_effect(map, effect)
					if effect.is_prevented():
						return effect
	return effect

static func apply_effect(map: Map, entity: Entity, effect: Effect):
	var applied_rules := []
	for property in entity.get_children():
		if property is Property:
			for rule in property.get_children():
				if rule is Rule:
					if rule.APPLIES_EFFECT and rule.accepts(effect):
						applied_rules.append(rule)
	if entity.DEBUG_EFFECTS and applied_rules.size() >= 1:
		print(effect.dump())
		print("applied by:")
		for rule in applied_rules:
			print("\t%s" % rule.get_path())
	for rule in applied_rules:
		if rule is Rule:
			rule._apply_effect(map, effect)
