class_name TravelEffect extends Effect

static func _required_fields() -> Array:
	return [EffectData.TYPE.DIRECTION]

func get_class(): return "TravelEffect"

func towards(dir: int) -> TravelEffect:
	data.direction = dir
	return self

func get_direction() -> int:
	return data.direction
