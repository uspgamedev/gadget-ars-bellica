class_name BeginTurnEffect extends Effect

static func _required_fields() -> Array:
	return []

func get_class(): return "BeginTurnEffect"
