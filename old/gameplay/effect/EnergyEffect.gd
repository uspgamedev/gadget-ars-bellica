class_name EnergyEffect extends Effect

static func _required_fields() -> Array:
	return [
		EffectData.TYPE.VALUE,
		Element.TAG[Element.TYPE.IMPACT],
		Element.TAG[Element.TYPE.FIRE],
		Element.TAG[Element.TYPE.SHOCK],
		Element.TAG[Element.TYPE.LIFE],
	]

func get_class(): return "EnergyEffect"

func with_energy(energy: int = 1) -> EnergyEffect:
	data.value = energy
	return self

func with_element(element_type: int) -> EnergyEffect:
	data.flags[Element.TAG[element_type]] = true
	return self

func get_energy() -> int:
	return data.value

func has_element(element_type: int) -> bool:
	return data.flags.get(Element.TAG[element_type], false)
