class_name MoveEffect extends Effect

const RELATIVE := "relative"

static func _required_fields() -> Array:
	return [
		EffectData.TYPE.TILE,
		EffectData.TYPE.MOVEMENT_PROFILE,
		RELATIVE
	]

func get_class(): return "MoveEffect"

func _init():
	data.movement_profile = MovementProfile.new()

func to(tile: Vector2) -> MoveEffect:
	data.tile = tile
	return self

func with_profile(profile: MovementProfile) -> MoveEffect:
	data.movement_profile = profile
	return self

func get_tile() -> Vector2:
	return data.tile

func get_movement_profile() -> MovementProfile:
	return data.movement_profile

func is_relative() -> bool:
	return data.has_flag(RELATIVE)
