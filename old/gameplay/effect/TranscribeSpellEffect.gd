class_name TranscribeSpellEffect extends Effect

static func _required_fields() -> Array:
	return [EffectData.TYPE.SCENE]

func get_class(): return "TranscribeSpellEffect"

func transcribe_spell(spell_scn: PackedScene) -> TranscribeSpellEffect:
	data.scene = spell_scn
	return self

func get_spell() -> PackedScene:
	return data.scene
