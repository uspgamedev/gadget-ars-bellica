class_name Traveler extends Property

onready var travel := AbilityHelper.find_ability(self)

func _available_abilities(_map: Map) -> Array:
	var entity := get_node(owner_path) as Entity
	var abilities := []
	if entity.tile.y == 0:
		abilities.append({
			name = "Travel", ability = travel, data = {
				AbilityTarget.FIELD.TARGET_DIR: Tile.DIR.UP
			}
		})
	if entity.tile.y == Map.SIZE - 1:
		abilities.append({
			name = "Travel", ability = travel, data = {
				AbilityTarget.FIELD.TARGET_DIR: Tile.DIR.DOWN
			}
		})
	if entity.tile.x == 0:
		abilities.append({
			name = "Travel", ability = travel, data = {
				AbilityTarget.FIELD.TARGET_DIR: Tile.DIR.LEFT
			}
		})
	if entity.tile.x == Map.SIZE - 1:
		abilities.append({
			name = "Travel", ability = travel, data = {
				AbilityTarget.FIELD.TARGET_DIR: Tile.DIR.RIGHT
			}
		})
	return abilities
