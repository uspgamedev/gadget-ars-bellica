class_name Spawn extends Property

export var spawner_path := NodePath()

var spawner_name := "an unknown source"

func _ready():
	var spawner := get_node_or_null(spawner_path) as Entity
	if spawner != null:
		spawner_name = spawner.entity_name

func _process(_delta):
	INFO = "spawned by %s" % spawner_name.to_upper()

func _save(storage: Dictionary):
	storage["spawner_path"] = spawner_path
