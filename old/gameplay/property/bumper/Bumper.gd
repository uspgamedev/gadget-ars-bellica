class_name Bumper extends AI

onready var bump_ability := AbilityHelper.find_ability(self)

func _process_property(entity: Entity, map: Map):
	var actor := entity.get_property(Actor) as Actor
	if actor.next_action == null:
		var target = find_nearest(entity)
		if target == null: return
		var entity_tile = entity.tile
		var dist = Tile.distance(target.tile, entity_tile)
		if dist > 1: return
		var bump := AbilityUse \
			.new(bump_ability, map, entity) \
			.with_target_tile(target.tile)
		actor.next_action = bump
