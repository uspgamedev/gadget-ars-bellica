class_name Property extends Node2D

export(String, MULTILINE) var INFO = ""

var owner_path := NodePath("")

func _entity_ready(entity: Entity):
	self.owner_path = entity.get_path()
	for child in get_children():
		if child.has_method("_property_ready"):
			child._property_ready(entity, self)

# Should return an array of ditionaries with fields:
#   "name" - A string that identifies the ability
#   "ability" - Ability object
#   "data" - Dictionary with default data values for ability
# NOTE: map cannot be typed here because it causes a cyclic dependency
func _available_abilities(_map) -> Array:
	return []

func get_owner_entity() -> Entity:
	if has_node(owner_path):
		return get_node(owner_path) as Entity
	else:
		return null

func _save(_storage: Dictionary):
	pass
