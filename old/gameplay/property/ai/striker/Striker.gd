class_name Striker extends AI

export(int, 1, 100) var strike_chance = 100
export(int, 0, 100) var cooldown = 0

onready var strike_ability := AbilityHelper.find_ability(self)
onready var timer := 0

func _process_property(entity: Entity, map: Map):
	if timer > 0:
		return
	var actor := entity.get_property(Actor) as Actor
	if actor.next_action == null and randf() * 100 <= strike_chance:
			var target = find_nearest(entity)
			if target == null: return
			var target_tile := target.tile as Vector2
			var strike := AbilityUse \
				.new(strike_ability, map, entity) \
				.with_target_tile(target_tile)
			if strike.pending_command() == null:
				actor.next_action = strike
				timer += cooldown + 1
