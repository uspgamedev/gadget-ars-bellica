extends Rule

func _apply_effect(_map: Map, _effect: Effect):
	var striker := get_self_property() as Striker
	striker.timer = max(striker.timer - 1, 0) as int
