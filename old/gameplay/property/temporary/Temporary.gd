class_name Temporary extends Property

export var duration := 1

func _process(_delta):
	INFO = "temporary (%d rounds left)" % duration

func _save(storage: Dictionary):
	storage["duration"] = duration
