extends Rule

func _process_effect(_map: Map, effect: Effect) -> Effect:
	var temporary := get_self_property() as Temporary
	if temporary.duration <= 0:
		effect = effect \
			.prevent() \
			.then(DestroyEffect.new().on(get_self_entity()))
	return effect

func _apply_effect(_map: Map, _effect: Effect):
	var temporary := get_self_property() as Temporary
	temporary.duration = max(temporary.duration - 1, 0) as int
