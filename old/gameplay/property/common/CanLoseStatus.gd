extends Rule

signal message(text)

func _apply_effect(_map: Map, effect: Effect):
	var remove_effect := effect as RemoveStatusEffect
	var status := get_node(remove_effect.get_status_path()) as Status
	if get_self_entity().is_a_parent_of(status):
		status.queue_free()
		if status.INFO != "":
			emit_signal(
				"message",
				"%s is no longer %s." % [
					get_self_entity().entity_name,
					status.INFO
				]
			)
