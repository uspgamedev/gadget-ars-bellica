extends Rule

func _process_effect(map: Map, effect: Effect) -> Effect:
	var target_effect := effect as TargetEffect
	var target_entity := get_node(target_effect.get_target_entity_path()) as Entity
	var target_profile := target_effect.get_movement_profile()
	if Physics.new(map).hits(target_profile, target_entity):
		var caused_effect := target_effect.get_caused_effect().on(target_entity)
		effect = effect.then(caused_effect)
		return effect
	else:
		return effect.prevent()
