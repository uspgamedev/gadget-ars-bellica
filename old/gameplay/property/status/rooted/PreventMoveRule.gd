extends Rule

func _process_effect(_map: Map, effect: Effect) -> Effect:
	var move_effect := effect as MoveEffect
	return move_effect.to(move_effect.get_entity().tile)
