tool
class_name UniqueStatusPerSource extends Rule

func _ready():
	EFFECT_TYPES = [GiveStatusEffect]
	PROCESSES_EFFECT = true

func _process_effect(_map: Map, effect: Effect) -> Effect:
	var status_effect := effect as GiveStatusEffect
	var status := get_self_property() as Status
	var status_scn_path := status.filename
	var new_status_scn := status_effect.get_status()
	var source_path := status_effect.get_source_path()
	if new_status_scn.resource_path == status_scn_path:
		if status.SOURCE_ENTITY_PATH == source_path:
			return effect.prevent()
	return effect
