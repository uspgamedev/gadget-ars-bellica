class_name Status extends Property

export(int) var DURATION := 1
export(NodePath) var SOURCE_ENTITY_PATH

func get_source() -> Entity:
	return get_node_or_null(SOURCE_ENTITY_PATH) as Entity

func _tick():
	if DURATION < 0: return false
	DURATION = int(max(0, DURATION - 1))
	return DURATION == 0

func _save(storage: Dictionary):
	storage["DURATION"] = DURATION
	storage["SOURCE_ENTITY_PATH"] = SOURCE_ENTITY_PATH
