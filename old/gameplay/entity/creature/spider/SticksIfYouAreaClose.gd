extends TriggerRule

func _apply_effect(map: Map, effect: Effect):
	var sense_effect := effect as SenseEffect
	var sensed_entity := get_node(sense_effect.get_subject_path()) as Entity
	if		sense_effect.data.has_flag(AreaSensor.OTHER_ENTERED) \
		or	sense_effect.data.has_flag(AreaSensor.SELF_ENTERED):
			if not is_already_stuck(sensed_entity):
				trigger_ability(map, {
					AbilityTarget.FIELD.TRIGGERING_ENTITY: sensed_entity.get_path()
				})

func is_already_stuck(entity: Entity) -> bool:
	for status in entity.get_children():
		if status is Status:
			if status.get_source() == get_self_entity():
				return true
	return false
