extends Property

const DESCR := "lore (%s)"
const PROMPT := "RECORD"

export var spell_scn: PackedScene
export var cmd_path: NodePath

func _entity_ready(entity: Entity):
	var cmd := get_node(cmd_path) as NewEffect
	cmd.scene = spell_scn
	entity.get_property(Interactive).interaction_prompt = \
		PROMPT

func get_spell_name() -> String:
	return spell_scn.get_state().get_node_property_value(0, 0)

func _process(_delta):
	INFO = DESCR % get_spell_name()

func _save(storage: Dictionary):
	storage["spell_scn"] = spell_scn
