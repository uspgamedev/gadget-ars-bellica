class_name Player extends Property

# Follors same order as Tile.DIR
onready var dir_hints := [
	$Up, $Down, $Left, $Right,
	$UpLeft, $DownRight, $UpRight, $DownLeft
]

onready var show_dir_hint := false

func _ready():
	add_to_group(Entity.PLAYER_GROUP)

func _process_property(entity: Entity, map: Map):
	if show_dir_hint:
		var cursor_tile := map.cursor_controller.map_position
		var dir := Tile.to_closest_dir(cursor_tile - entity.tile)
		var physics := Physics.new(map)
		for idx in dir_hints.size():
			var target_tile := entity.tile + Tile.FROM_DIR[dir] as Vector2
			if idx == dir and physics.can_enter_tile(entity, target_tile):
				dir_hints[idx].show()
			else:
				dir_hints[idx].hide()
	else:
		for hint in dir_hints:
			hint.hide()
