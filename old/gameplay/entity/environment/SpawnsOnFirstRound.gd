extends Rule

func _process_effect(map: Map, effect: Effect) -> Effect:
	var spawner := get_self_property() as RandomSpawner
	if spawner.spawn_done:
		return effect
	spawner.spawn_done = true
	var biome := map.biome as Biome
	for gen in biome.entity_generators:
		var generator := gen as EntityGenerator
		var generated := generator.generate(map)
		for spawn in generated:
			var self_entity = get_self_entity()
			var tile := spawn.tile as Vector2
			var spawn_effect := SpawnEffect.new() \
				.spawn_entity(spawn.entity_scn) \
				.with_auto_spill() \
				.at(tile) \
				.on(self_entity)
			effect = effect.then(spawn_effect)
	return effect
