extends Rule

func _apply_effect(_map: Map, effect: Effect):
	var change_pos_effect := effect as ChangePositionEffect
	var moved_path := change_pos_effect.get_moved_entity_path()
	var moved_entity := get_node_or_null(moved_path) as Entity
	if moved_entity != null:
		moved_entity.tile = change_pos_effect.get_tile()
