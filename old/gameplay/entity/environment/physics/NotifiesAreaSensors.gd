extends Rule

func _process_effect(map: Map, effect: Effect) -> Effect:
	var position_effect := effect as ChangePositionEffect
	# Notify OTHER sensor entities of moving entity 
	var target_tile := position_effect.get_tile()
	var moving_path := position_effect.get_moved_entity_path()
	var moving_entity := get_node(moving_path) as Entity
	for sensor in get_tree().get_nodes_in_group(Entity.SENSOR_GROUP):
		if not sensor is Entity or sensor == moving_entity:
			continue
		var sensor_tile := (sensor as Entity).tile
		var sensor_radius := sensor.get_property(AreaSensor).radius as int
		# Entity moved INTO sensor
		if Tile.distance(target_tile, sensor_tile) <= sensor_radius:
			var sense_effect := SenseEffect.new() \
				.senses(position_effect) \
				.about(moving_entity) \
				.on(sensor)
			sense_effect.data.set_flag(AreaSensor.OTHER_ENTERED)
			effect = sense_effect.then(effect)
		# Entity moved OUT OF sensor
		elif Tile.distance(moving_entity.tile, sensor_tile) <= sensor_radius:
			var sense_effect := SenseEffect.new() \
				.senses(position_effect) \
				.about(moving_entity) \
				.on(sensor)
			sense_effect.data.set_flag(AreaSensor.OTHER_EXITED)
			effect = sense_effect.then(effect)
	# Notify MOVING sensor entity of other entities
	var sensor := moving_entity.get_property(AreaSensor) as AreaSensor
	if sensor != null:
		for entity in map.get_all_entities():
			if entity == moving_entity:
				continue
			if Tile.distance(entity.tile, target_tile) <= sensor.radius:
				var sense_effect := SenseEffect.new() \
					.senses(position_effect) \
					.about(entity) \
					.on(moving_entity)
				sense_effect.data.set_flag(AreaSensor.SELF_ENTERED)
				effect = sense_effect.then(effect)
			elif Tile.distance(entity.tile, moving_entity.tile) <= sensor.radius:
				var sense_effect := SenseEffect.new() \
					.senses(position_effect) \
					.about(entity) \
					.on(moving_entity)
				sense_effect.data.set_flag(AreaSensor.SELF_EXITED)
				effect = sense_effect.then(effect)
	return effect
