extends Rule

export var SPAWN_PROPERTY_SCN: PackedScene

func _process_effect(map: Map, effect: Effect) -> Effect:
	var spawn_effect := effect as SpawnEffect
	var spawned := spawn_effect.get_spawned_entity()
	var tile := spawn_effect.get_spawn_tile()
	var physics := Physics.new(map)
	if not physics.can_enter_tile(spawned, tile):
		if spawn_effect.is_auto_spill():
			var unblocked := []
			for dir in Tile.DIR.ALL:
				var new_tile = tile + Tile.FROM_DIR[dir]
				if physics.can_enter_tile(spawned, new_tile):
					unblocked.append(new_tile)
			if not unblocked.empty():
				var new_tile = unblocked[randi() % unblocked.size()]
				spawn_effect = spawn_effect.at(new_tile)
			else:
				return effect.prevent()
		else:
			return effect.prevent()
	return effect

func _apply_effect(map: Map, effect: Effect):
	var spawn_effect := effect as SpawnEffect
	var spawned := spawn_effect.get_spawned_entity()
	var spawner_entity := get_node_or_null(spawn_effect.get_spawner_path()) as Entity
	var tile := spawn_effect.get_spawn_tile()
	if spawn_effect.has_dir() and spawned.has_property(Direction):
		var direction := spawn_effect.get_spawn_dir()
		spawned.get_property(Direction).direction = direction
	map.add_entity_at(spawned, tile)
	if spawner_entity != null:
		if spawner_entity.is_in_group(Entity.PLAYER_GROUP):
			spawned.add_to_group(Entity.PLAYER_GROUP)
		if spawn_effect.is_controlled() and spawner_entity.has_property(Controller):	
			var spawn_property := SPAWN_PROPERTY_SCN.instance() as Spawn
			spawn_property.spawner_path = spawner_entity.get_path()
			spawned.add_property(spawn_property, true)
			spawner_entity.get_property(Controller)\
				.controlled_paths\
				.append(spawned.get_path())
	var pos_effect := ChangePositionEffect.new() \
		.relocate(spawned) \
		.to(tile) \
		.on(get_self_entity())
	effect = effect.then(pos_effect)

