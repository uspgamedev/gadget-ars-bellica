class_name RandomSpawner extends Property

export var spawn_done := false

func _save(storage: Dictionary):
	storage["spawn_done"] = spawn_done
