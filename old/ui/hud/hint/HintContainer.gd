class_name HintContainer extends PanelContainer

enum {
	HIDDEN, VISIBLE, FADING_OUT
}

export var LABEL_PATH: NodePath
export var INFO_PATH: NodePath
export var OFFSET := 0.5
export var SMOOTH := 5.0

const MARGIN = Vector2(24, 24)
const UNDEFINED_HINT = "undefined"

onready var label: Label = get_node(LABEL_PATH) as Label
onready var info_label := get_node(INFO_PATH) as Label
onready var target_position := Vector2()
onready var current_tile := -Vector2.ONE
onready var timer: Timer = $Timer

var cursor_hint_enabled := true
var hint_state_dirty := true
var state: int = HIDDEN

func _ready():
	hide()
	yield(get_tree(), "physics_frame")
	rect_position = center()
	target_position = center()

func get_parent_size() -> Vector2:
	return (get_parent() as Control).rect_size

func center() -> Vector2:
	return get_parent_size() / 2 - rect_size / 2

func enable() -> void:
	if not self.cursor_hint_enabled:
		self.hint_state_dirty = true
	self.cursor_hint_enabled = true

func disable() -> void:
	if self.cursor_hint_enabled:
		self.hint_state_dirty = true
	self.cursor_hint_enabled = false

func set_selected_tile(tile_position: Vector2, info: String):
	if self.label.text != UNDEFINED_HINT and self.cursor_hint_enabled:
		if tile_position != self.current_tile or self.hint_state_dirty:
			self.current_tile = tile_position
#			fade_out()
			SignalHelper.reconnect(self.timer, "timeout", self, "fade_in", \
								   [info])
			self.timer.start()
			self.hint_state_dirty = false
	else:
		fade_out()
		SignalHelper.safe_disconnect(self.timer, "timeout", self, "fade_in")

func calculate_target_position() -> Vector2:
	var unit = Tile.SIZE * 2
	var local_position = current_tile * unit + MARGIN
	var parent_size = get_parent_size()
	var pivot = local_position + Vector2(unit, unit) * 0.5
	if pivot.x > parent_size.x * 0.5:
		local_position.x -= self.rect_size.x + unit * (OFFSET + 1.1)
	else:
		local_position.x += unit * (OFFSET + 1.1)
	if pivot.y > parent_size.y * 0.5:
		local_position.y -= self.rect_size.y + unit * (OFFSET + 0.1)
	else:
		local_position.y += unit * (OFFSET + 1.1)
	return local_position

func fade_in(info: String):
	state = VISIBLE
	info_label.hide()
	if info != "":
		info_label.text = info
		info_label.show()
	hide()
	show()
	propagate_call("set_size", [Vector2()])
#	modulate = Color.transparent
	target_position = calculate_target_position()
	var delay := (Color.white.a - modulate.a) * 0.5
	SignalHelper.safe_disconnect($Tween, "tween_all_completed", self, \
								 "_after_fade_out")
	$Tween.stop_all()
	$Tween.interpolate_property(self, "modulate", modulate, \
								Color.white, delay, Tween.TRANS_CUBIC, \
								Tween.EASE_OUT)
	$Tween.start()

func fade_out():
	if state == VISIBLE:
		state = FADING_OUT
		$Tween.interpolate_property(self, "modulate", Color.white, \
									Color.transparent, 0.5, Tween.TRANS_CUBIC, \
									Tween.EASE_OUT)
		$Tween.start()
		SignalHelper.reconnect($Tween, "tween_all_completed", self, \
							   "_after_fade_out", [], CONNECT_ONESHOT)

func _after_fade_out():
	if state == FADING_OUT:
		state = HIDDEN
		target_position = center()
		hide()

func _process(delta):
	rect_position += (target_position - rect_position) * SMOOTH * delta

func set_hint(hint: String):
	# force resize of bounding box
	SignalHelper.safe_connect(get_tree(), "physics_frame", self, \
							  "_set_hint", [hint])

func _set_hint(hint: String):
	self.rect_size = self.rect_min_size
	label.text = hint

func _exit_tree():
	SignalHelper.safe_disconnect(get_tree(), "physics_frame", self, \
								 "_set_hint")
