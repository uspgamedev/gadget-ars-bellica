extends Label

func _update_info(battle: Battle):
	text = ""
	for path in battle.get_current_turn_order():
		var entity := get_node_or_null(path) as Entity
		if entity != null:
			text += "  %s\n" % entity.entity_name
