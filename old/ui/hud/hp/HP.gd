extends HBoxContainer

export var HEART_SCN: PackedScene
export var HP_PER_HEART := 2

func _update_info(battle: Battle):
	var player := battle.get_map().get_player()
	var destructible := player.get_property(Destructible) as Destructible
	var hp := destructible.get_hp()
	var max_hp := destructible.max_hp as int
	var max_hearts := max_hp / HP_PER_HEART
	for heart_idx in range(max_hearts):
		if heart_idx >= get_child_count():
			add_child(HEART_SCN.instance())
		if heart_idx < hp / HP_PER_HEART:
			get_child(heart_idx).value = HP_PER_HEART
		elif heart_idx < hp / HP_PER_HEART + 1:
			get_child(heart_idx).value = hp % HP_PER_HEART
		else:
			get_child(heart_idx).value = 0

