class_name SpellButton extends TextureButton

var spell_path: NodePath

signal spell_selected(spell)
signal spell_erased(spell)

func _ready():
	if has_node(self.spell_path):
		$Icon.texture = get_node(self.spell_path).spell_icon

func _on_use_spell():
	if has_node(self.spell_path):
		var spell := get_node(self.spell_path) as Spell
		emit_signal("spell_selected", spell)

func _process(_delta):
	if has_node(self.spell_path):
		$Icon.texture = get_node(self.spell_path).spell_icon
		disabled = false
	else:
		$Icon.texture = null
		disabled = true

func _on_erase_spell():
	if has_node(self.spell_path):
		var spell := get_node(self.spell_path) as Spell
		emit_signal("spell_erased", spell)

func get_spell() -> Spell:
	return get_node_or_null(spell_path) as Spell
