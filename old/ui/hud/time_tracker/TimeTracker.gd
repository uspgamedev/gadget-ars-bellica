extends HBoxContainer

func _update_info(battle: Battle):
	var player := battle.get_map().get_player()
	var actor := player.get_property(Actor) as Actor
	var ap := actor.action_points
	var mid := Actor.ACTION_COST
	for idx in range(Actor.ACTION_COST):
		if ap > 0:
			get_child(mid - idx - 1).filled = false
			if idx < ap:
				get_child(mid + idx + 1).filled = true
			else:
				get_child(mid + idx + 1).filled = false
		elif ap < 0:
			get_child(mid + idx + 1).filled = false
			if idx < -ap:
				get_child(mid - idx - 1).filled = true
			else:
				get_child(mid - idx - 1).filled = false
		else:
			get_child(mid + idx + 1).filled = false
			get_child(mid - idx - 1).filled = false
	if ap <= -Actor.ACTION_COST:
		$Hourglass.modulate = Color.red
	elif ap >= Actor.ACTION_COST:
		$Hourglass.modulate = Color.green
	else:
		$Hourglass.modulate = Color.white
