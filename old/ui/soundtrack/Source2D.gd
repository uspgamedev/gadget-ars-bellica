extends AudioStreamPlayer2D

func _ready():
	stream.mix_rate = Instrument.SAMPLE_RATE
	_fill_buffer() # Prefill, do before play() to avoid delay.
	play()
	yield(get_tree().create_timer(1.0), "timeout")
	$AnimationPlayer.play("test")

func _process(_delta):
	_fill_buffer()

func _fill_buffer():
	var playback := get_stream_playback()
	var to_fill = playback.get_frames_available()
	if to_fill < DSPBlock.BLOCK_SIZE:
		return
	
	var block := DSPBlock.new()
	
	for child in get_children():
		if child is DSPNode:
			child.fill_block(block)
	
	block.flush(playback)
	
	yield(get_tree().create_timer(0.6), "timeout")
	var half = DSPBlock.BLOCK_SIZE / 2
	for i in DSPBlock.BLOCK_SIZE:
		$Line2D.points[i].x = -half + i
		$Line2D.points[i].y = - 100 * block.samples[i].x
	
	block.free()
