extends Control

signal new_game_requested
signal continue_requested

var has_saved_game := false

func _ready():
	if !has_saved_game:
		$MenuBox/ContinueButton.hide()

func _on_continue_pressed():
	emit_signal("continue_requested")

func _on_new_game_pressed():
	emit_signal("new_game_requested")

func _on_exit_pressed():
	get_tree().quit()
