extends Control

var simulation: Simulation
var player_id: int
var target_tile_hex: Vector2

func start(the_simulation: Simulation, the_player_id: int,
		   the_target_tile_hex: Vector2):
	simulation = the_simulation
	player_id = the_player_id
	target_tile_hex = the_target_tile_hex

# ex: has options for Doing Nothing, Moving, Collecting Resource, Interacting
# and Using the Currently Equipped Gadget
