class_name UI extends Control

export var main_menu_scn: PackedScene

onready var main_menu := $MainMenu as MainMenu

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		$Requester.request("exit", Request.new())
